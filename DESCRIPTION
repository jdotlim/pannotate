Package: pannotate
Type: Package
Title: Prokaryotic Protein Annotator
Version: 0.0.1
Date: 2022-03-16
Author: Justin Lim
Maintainer: Justin Lim <justlim@mit.edu>
Description: Implements a workflow to annotate prokaryotic proteins using
    sequences homology and HMM comparisons. Proteins are first compared
    against a set of high quality, high confidence databases that can be used
    to name the protein or assign function. Any protein that cannot be
    annotated with these databases is then annotated with protein domains from
    Pfam-A. Annotation speed is improved by using multiple threads and only
    annotating each protein with a single database.
License: MIT + file LICENSE
Encoding: UTF-8
SystemRequirements: HMMER (>= 3.2.2), SWORD (>= 1.0.4)
Depends: R (>= 4.1), data.table
Imports: utils, curl, processx, parallel, Biostrings
Suggests: tinytest
RoxygenNote: 7.1.2
